#include "network.h"
#include "detection_layer.h"
#include "region_layer.h"
#include "cost_layer.h"
#include "utils.h"
#include "parser.h"
#include "box.h"
#include "image.h"
#include "demo.h"
#include "darknet.h"
#ifdef WIN32
#include <time.h>
#include "gettimeofday.h"
#else
#include <sys/time.h>
#endif


#ifdef OPENCV

#include "http_stream.h"

// JH EDIT
#define MULTI_TEST 1
#define MULTI_VIDEO 2
#define MULTI_NET 3

static int flag_fetch[17] = { 0, };
/////////////////////

static char **demo_names;
static image **demo_alphabet;
static int demo_classes;

static int nboxes = 0;
static detection *dets = NULL;

static network net;
static image in_s;
static image in_s_video[17];
static image det_s;

static cap_cv *cap;
static cap_cv *cap2;
static cap_cv *cap3;
static cap_cv *cap4;
static float fps = 0;
static float demo_thresh = 0;
static int demo_ext_output = 0;
static long long int frame_id = 0;
static int demo_json_port = -1;
static int demo_multi_channel_flag = 0;
static int demo_multi_channel = 0;
static int demo_rtp_socket = 0;
static int demo_tcp_socket = 0;

int cam_w;
int cam_h;
char camSize[20] = { 0, };
char rectInfo[1000];
char finalRectInfo[1000] = { 0, };
int rect_flag = 0;


#define NFRAMES 3

static float* predictions[NFRAMES];
static int demo_index = 0;
static mat_cv* cv_images[NFRAMES];
static float *avg;

mat_cv* in_img;
//mat_cv* in_img_video[17];
mat_cv* in_img1;
mat_cv* in_img2;
mat_cv* in_img3;
mat_cv* in_img4;
mat_cv* det_img;
mat_cv* show_img;

static volatile int flag_exit;
static int letter_box = 0;


// SOCKET ///
#define CHECK_CONNECTION_REQUEST 0X10
#define CHECK_CONNECTION_RESPONSE 0X11
#define MAIN_CONNECTION_REQUEST 0X20
#define MAIN_CONNECTION_RESPONSE 0X21
#define INSTANCE_CONNECTION_REQUEST 0X30
#define INSTANCE_CONNECTION_RESPONSE 0X31
#define EVENT_REPORT 0x07
#define EVENT_REPORT_START 0x40
#define EVENT_REPORT_STOP 0x41
#define STX 0x02
#define ETX 0x03
int flag_report = 0;
pthread_t socket_thread;
#define BUF_LEN 1024
char buffer[BUF_LEN];
char msg[BUF_LEN - 3];
int msgSize = 0;
WSADATA wsaData;
SOCKADDR_IN server_addr, client_addr;
SOCKET server_fd, client_fd;
int len, msg_size;
char temp[20];
DWORD t_out;
int reuseflag = TRUE;
void initializeSocket(int tcp_port);
int parseMsg(char* buffer, int msg_size, char *message);
void sendChar(int client_fd, char msg);
void sendMsg(int client_fd, char* msg, char type);
//////////////

////THREAD /////////
pthread_mutex_t mutex_lock;
///////////////////////

/*
void *fetch_in_thread(void *StreamId)
{
	int nStream;
	if (StreamId == NULL) {

	}
	else {
		nStream = *(int*)StreamId;
	}
	int dont_close_stream = 1;    // set 1 if your IP-camera periodically turns off and turns on video-stream
	if (letter_box) {
		in_s = get_image_from_stream_letterbox(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
	}
	else {
		in_s = get_image_from_stream_resize(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
	}
	if(!in_s.data){
		printf("Stream closed.\n");
		flag_exit = 1;
		//exit(EXIT_FAILURE);
		return 0;
	}
	//in_s = resize_image(in, net.w, net.h);

	// Concat Module
	if(demo_multi_channel>1 && demo_multi_channel_flag==MULTI_TEST)  in_s = get_concat_image(net.w, net.h, net.c, &in_img,demo_multi_channel,in_s);

	return 0;
}
*/
void *fetch_in_thread(void *StreamId)
{
	int nStream;
	int dont_close_stream = 0;    // set 1 if your IP-camera periodically turns off and turns on video-stream
	if (StreamId == NULL) {
		if (letter_box) {
			in_s = get_image_from_stream_letterbox(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
		}
		else {
			if (demo_multi_channel_flag == MULTI_VIDEO) {
				in_s = get_image_from_stream_resize_video(cap, cap2, cap3, cap4, net.w, net.h, net.c, &in_img, dont_close_stream, demo_multi_channel);
			}
			else {
				if (demo_tcp_socket) {
					in_s = get_image_from_stream_resize_viva_demo(cap, net.w, net.h, net.c, &in_img, dont_close_stream, &cam_w, &cam_h);
				}
				else {

					in_s = get_image_from_stream_resize(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
				}
			}
		}
		if (!in_s.data) {
			printf("Stream closed.\n");
			flag_exit = 1;
			//exit(EXIT_FAILURE);
			return 0;
		}
		//in_s = resize_image(in, net.w, net.h);

		// Concat Module
		if (demo_multi_channel > 1 && demo_multi_channel_flag == MULTI_TEST)  in_s = get_concat_image(net.w, net.h, net.c, &in_img, demo_multi_channel, in_s);

		return 0;
	}
	else {
		nStream = *(int*)StreamId;
		printf("\nGettin Images.. %d\n", nStream);
		switch (nStream) {
			/*case 1:in_s_video[nStream] = get_image_from_stream_resize(cap,  net.w, net.h, net.c, &in_img_video[nStream], dont_close_stream); break;
			case 2:in_s_video[nStream] = get_image_from_stream_resize(cap2, net.w, net.h, net.c, &in_img_video[nStream], dont_close_stream); break;
			case 3:in_s_video[nStream] = get_image_from_stream_resize(cap3, net.w, net.h, net.c, &in_img_video[nStream], dont_close_stream); break;
			case 4:in_s_video[nStream] = get_image_from_stream_resize(cap4, net.w, net.h, net.c, &in_img_video[nStream], dont_close_stream); break;*/
		case 1:in_s_video[nStream] = get_image_from_stream_resize(cap, net.w, net.h, net.c, &in_img1, dont_close_stream); break;
		case 2:in_s_video[nStream] = get_image_from_stream_resize(cap2, net.w, net.h, net.c, &in_img2, dont_close_stream); break;
		case 3:in_s_video[nStream] = get_image_from_stream_resize(cap3, net.w, net.h, net.c, &in_img3, dont_close_stream); break;
		case 4:in_s_video[nStream] = get_image_from_stream_resize(cap4, net.w, net.h, net.c, &in_img4, dont_close_stream); break;
		}
		if (!in_s_video[nStream].data) {
			printf("Stream closed.\n");
			flag_exit = 1;
			//exit(EXIT_FAILURE);
			return 0;
		}
		printf("\nGot Image.. %d\n", nStream);

		flag_fetch[nStream] = 1;
	}


}
void *concat_in_thread(void *ptr) {
	if (demo_multi_channel_flag == MULTI_VIDEO) {
		printf("\n concatinating.. \n");
		in_s = get_concat_video_image(net.w, net.h, net.c, &in_img, in_img1, in_img2, in_img3, in_img4, demo_multi_channel, in_s, in_s_video, flag_fetch);
		printf("\n concat done.. \n");
	}
	else if (demo_multi_channel_flag == MULTI_NET) {
	}
}
void *detect_in_thread(void *ptr)
{
	layer l = net.layers[net.n - 1];
	float *X = det_s.data;
	float *prediction = network_predict(net, X);

	memcpy(predictions[demo_index], prediction, l.outputs * sizeof(float));
	mean_arrays(predictions, NFRAMES, l.outputs, avg);
	l.output = avg;

	free_image(det_s);

	cv_images[demo_index] = det_img;
	det_img = cv_images[(demo_index + NFRAMES / 2 + 1) % NFRAMES];
	demo_index = (demo_index + 1) % NFRAMES;

	if (letter_box)
		dets = get_network_boxes(&net, get_width_mat(in_img), get_height_mat(in_img), demo_thresh, demo_thresh, 0, 1, &nboxes, 1); // letter box
	else
		dets = get_network_boxes(&net, net.w, net.h, demo_thresh, demo_thresh, 0, 1, &nboxes, 0); // resized

	return 0;
}
void* socket_in_thread(void *arg) {
	int tcp_port;
	if (demo_rtp_socket) {
		tcp_port = *(int *)arg + 1000;
	}
	if (demo_tcp_socket) {
		tcp_port = *(int *)arg;
	}
	int programDone = 0;
	int heartBeat = 0;
	int engCount = 0;
	int leng = 0;
	printf("YOLO : INITIALIZE, TCP: %d", tcp_port);
	initializeSocket(tcp_port);
	printf("Socket Initialized , TCP : %d\n ", tcp_port);
	memset(buffer, 0x00, sizeof(buffer));
	memset(msg, 0x00, sizeof(msg));
	//leng = recv(client_fd, buffer, BUF_LEN, 0);
	//if (leng == 0) {
	//    printf("YOLO : Connection Closed normally \n");
	//    programDone = 1;
	//    flag_exit = 1;
	//}
	//else if (leng < 0) {
	//    //printf("?? bf while\n");
	//}
	//else if (leng > 0) {
	//    if (-1 == parseMsg(buffer, msg_size, msg)) {
	//        printf("YOLO : Parsing Error, check the protocol. \n");
	//    }
	//    else {
	//        if (msg[0] == ALIVE) {
	//            sendChar(client_fd, ACK);
	//            printf("YOLO : Sent ACK \n");
	//        }
	//    }
	//}
	engCount = 0;
	//sprintf(finalRectInfo, "%c%d,%d,%d,%d", 0x11, 1, 100, 50, 100);
	while (!programDone) {
		leng = recv(client_fd, buffer, BUF_LEN, 0);
		if (leng == 0) {
			printf("YOLO : Connection Closed normally\n");
			programDone = 1;
			flag_exit = 1;
		}
		else if (leng < 0) {
			int wsaErr = WSAGetLastError();
			if (wsaErr == WSAECONNRESET) {
				printf("YOLO : Connection Closed abnormally\n");
				programDone = 1;
				flag_exit = 1;
			}
			else if((wsaErr != WSAECONNRESET) && flag_report){
				pthread_mutex_lock(&mutex_lock);
				if (rect_flag) {
					//printf("%s\n", finalRectInfo);
					// send infomations
					sendMsg(client_fd, finalRectInfo, EVENT_REPORT);
					rect_flag = 0;
					//printf("report sent %s\n",finalRectInfo);
				}
				pthread_mutex_unlock(&mutex_lock);
			}
		}
		else if (leng > 0) {
			if (-1 == parseMsg(buffer, msg_size, msg)) {
				printf("YOLO : Parsing Error, check the protocol. \n");
			}
			else {
				if (msg[0] == INSTANCE_CONNECTION_REQUEST) {
					if (msg[1] == EVENT_REPORT_START) {
						flag_report = 1;
						sendMsg(client_fd, 0, msg[1]);
						printf("YOLO : Sent ACK for start \n");
					}
					else if (msg[1] == EVENT_REPORT_STOP) {
						flag_report = 0;
						sendMsg(client_fd, 0, msg[1]);
						printf("YOLO : Sent ACK for stop \n");
					}
					
				}
			}
		}
	}
}


void initializeSocket(int tcp_port) {

	WSAStartup(MAKEWORD(2, 2), &wsaData);
	t_out = 40;
	if ((server_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{// 소켓 생성
		printf("YOLO : Can't open stream socket\n");
		exit(0);
	}
	memset(&server_addr, 0x00, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(tcp_port);
	//server_addr 셋팅

	if (bind(server_fd, (SOCKADDR*)&server_addr, sizeof(server_addr)) < 0)
	{//bind() 호출
		printf("YOLO : Can't bind local address.\n");
		exit(0);
	}

	if (listen(server_fd, 5) < 0)
	{//소켓을 수동 대기모드로 설정
		printf("YOLO : Can't listening connect.\n");
		exit(0);
	}

	printf("YOLO : wating connection request. \n");
	len = sizeof(client_addr);
	client_fd = accept(server_fd, (SOCKADDR*)&client_addr, &len);
	setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&t_out, sizeof(t_out));
	setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&reuseflag, sizeof(reuseflag));
	if (client_fd < 0)
	{
		printf("YOLO : accept failed.\n");
		exit(0);
	}
	inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));
	printf("YOLO : %s client connected.\n", temp);
	return;
}

int parseMsg(char* buffer, int msg_size, char *message) {
	char i = 0;
	if (buffer[0] == STX) {
		for (; i < buffer[1]; i++) {
			message[i] = buffer[2 + i];
		}
		if (buffer[2 + i] == ETX) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 0;
	}
	return 1;
}

void sendChar(int client_fd, char msg) {
	char temp[BUF_LEN];
	memset(temp, 0x00, sizeof(temp));
	temp[0] = STX;
	temp[1] = 0x01;
	temp[2] = msg;
	temp[strlen(temp)] = ETX;
	send(client_fd, temp, strlen(temp) + 1, 0);
}
void sendMsg(int client_fd, char* msg, char type) {
	char temp[BUF_LEN];
	memset(temp, 0x00, sizeof(temp));
	temp[0] = STX;
	if (type == EVENT_REPORT) {
		if (demo_tcp_socket) {
			temp[1] = 1 + strlen(msg) + strlen(camSize);
			temp[2] = EVENT_REPORT;
			strcat(temp, camSize);
			strcat(temp, msg);
			temp[strlen(temp)] = ETX;
		}
		else {
			temp[1] = strlen(msg) + 1;
			temp[2] = EVENT_REPORT;
			strcat(temp, msg);
			temp[strlen(temp)] = ETX;
		}
	}
	else if ((type == EVENT_REPORT_START) || (type==EVENT_REPORT_STOP)) {
		temp[1] = 0x02;
		temp[2] = INSTANCE_CONNECTION_RESPONSE;
		temp[3] = type;
		temp[strlen(temp)] = ETX;
	}
	//printf("STX: %d, ETX: %d, LENGTH : %d, TOTAL : %d\n", STX, ETX, temp[1],strlen(temp));
	//write(client_fd, temp, sizeof(temp));
	//printf("MESSAGE: %s\n", temp);
	send(client_fd, temp, strlen(temp) + 1, 0);
}
double get_wall_time()
{
	struct timeval walltime;
	if (gettimeofday(&walltime, NULL)) {
		return 0;
	}
	return (double)walltime.tv_sec + (double)walltime.tv_usec * .000001;
}

void demo(char *cfgfile, char *weightfile, float thresh, float hier_thresh, int cam_index, const char *filename, char **names, int classes,
	int frame_skip, char *prefix, char *out_filename, int mjpeg_port, int json_port, int dont_show, int ext_output, int letter_box_in, int time_limit_sec, char *http_post_host, int multi_channel_flag, int multi_channel, int rtp_socket, int tcp_socket)
{
	letter_box = letter_box_in;
	in_img = det_img = show_img = NULL;
	//skip = frame_skip;
	image **alphabet = load_alphabet();
	int delay = frame_skip;
	demo_names = names;
	demo_alphabet = alphabet;
	demo_classes = classes;
	demo_thresh = thresh;
	demo_ext_output = ext_output;
	demo_json_port = json_port;
	demo_multi_channel = multi_channel;
	demo_multi_channel_flag = multi_channel_flag;
	demo_rtp_socket = rtp_socket;
	demo_tcp_socket = tcp_socket;
	printf("Demo\n");
	net = parse_network_cfg_custom(cfgfile, 1, 1);    // set batch=1
	if (weightfile) {
		load_weights(&net, weightfile);
	}
	fuse_conv_batchnorm(net);
	calculate_binary_weights(net);
	srand(2222222);
	printf("######### %d %d \n", net.w, net.h);
	pthread_mutex_init(&mutex_lock, NULL);
	memset(rectInfo, 0, sizeof(rectInfo));
	memset(finalRectInfo, 0x00, sizeof(finalRectInfo));
	memset(camSize, 0x00, sizeof(camSize));
	finalRectInfo[0] = 0x10;

	if (filename) {
		printf("video file: %s\n", filename);
		cap = get_capture_video_stream(filename);
		if (demo_multi_channel_flag == MULTI_VIDEO) {
			printf("\nOpening Vids\n");
			if (demo_multi_channel > 1) {
				cap2 = get_capture_video_stream("D:\\seminar\\test2.mp4");
				printf("video file: %s\n", "D:\\seminar\\test2.mp4");
			}
			if (demo_multi_channel > 2) {
				cap3 = get_capture_video_stream("D:\\seminar\\test3.mp4");
				printf("video file: %s\n", "D:\\seminar\\test3.mp4");
			}
			if (demo_multi_channel > 3) {
				cap4 = get_capture_video_stream("D:\\seminar\\test4.mp4");
				printf("video file: %s\n", "D:\\seminar\\test4.mp4");
			}
		}
	}
	else {
		printf("Webcam index: %d\n", cam_index);
		cap = get_capture_webcam(cam_index);
	}

	if (!cap) {
#ifdef WIN32
		printf("Check that you have copied file opencv_ffmpeg340_64.dll to the same directory where is darknet.exe \n");
#endif
		error("Couldn't connect to webcam.\n");
	}

	layer l = net.layers[net.n - 1];
	int j;

	avg = (float *)calloc(l.outputs, sizeof(float));
	for (j = 0; j < NFRAMES; ++j) predictions[j] = (float *)calloc(l.outputs, sizeof(float));

	if (l.classes != demo_classes) {
		printf("Parameters don't match: in cfg-file classes=%d, in data-file classes=%d \n", l.classes, demo_classes);
		getchar();
		exit(0);
	}


	flag_exit = 0;

	pthread_t fetch_thread[4];
	pthread_t concat_thread;
	pthread_t detect_thread;


	fetch_in_thread(0);
	det_img = in_img;
	det_s = in_s;

	fetch_in_thread(0);
	detect_in_thread(0);
	det_img = in_img;
	det_s = in_s;
	if (demo_rtp_socket) {
		if (pthread_create(&socket_thread, 0, socket_in_thread, &demo_rtp_socket)) error("Socket thread creation failed");
	}
	if (demo_tcp_socket) {
		if (pthread_create(&socket_thread, 0, socket_in_thread, &demo_tcp_socket)) error("Socket thread creation failed");
	}
	for (j = 0; j < NFRAMES / 2; ++j) {
		free_detections(dets, nboxes);
		fetch_in_thread(0);
		detect_in_thread(0);
		det_img = in_img;
		det_s = in_s;
	}

	int count = 0;
	if (!prefix && !dont_show) {
		int full_screen = 0;
		create_window_cv("Demo", full_screen, 640, 480);
	}


	write_cv* output_video_writer = NULL;
	if (out_filename && !flag_exit)
	{
		int src_fps = 25;
		src_fps = get_stream_fps_cpp_cv(cap);
		output_video_writer =
			create_video_writer(out_filename, 'D', 'I', 'V', 'X', src_fps, get_width_mat(det_img), get_height_mat(det_img), 1);

		//'H', '2', '6', '4'
		//'D', 'I', 'V', 'X'
		//'M', 'J', 'P', 'G'
		//'M', 'P', '4', 'V'
		//'M', 'P', '4', '2'
		//'X', 'V', 'I', 'D'
		//'W', 'M', 'V', '2'
	}

	int send_http_post_once = 0;
	const double start_time_lim = get_time_point();
	double before = get_wall_time();
	int V1 = 1;
	int V2 = 2;
	int V3 = 3;
	int V4 = 4;

	sprintf(camSize, "%d,%d,", cam_w, cam_h);
	printf("Test : camSize : %s\n", camSize);
	while (1) {
		++count;
		{
			const float nms = .45;    // 0.4F
			int local_nboxes = nboxes;
			detection *local_dets = dets;

			if (demo_multi_channel_flag == MULTI_TEST || demo_multi_channel_flag == 0) {
				if (pthread_create(&fetch_thread[0], 0, fetch_in_thread, 0)) error("Thread creation failed");
			}
			else if (demo_multi_channel_flag == MULTI_VIDEO) {
				printf("\nMakin Threads..\n");
				if (pthread_create(&fetch_thread[0], 0, fetch_in_thread, 0)) error("Thread creation failed1");
				/*if (demo_multi_channel > 1) {
					if (pthread_create(&fetch_thread[1], 0, fetch_in_thread, &V2)) error("Thread creation failed2");
				}
				if (demo_multi_channel > 2) {
					if (pthread_create(&fetch_thread[2], 0, fetch_in_thread, &V3)) error("Thread creation failed3");
				}
				if (demo_multi_channel > 3) {
					if (pthread_create(&fetch_thread[3], 0, fetch_in_thread, &V4)) error("Thread creation failed4");
				}*/
				//if (pthread_create(&concat_thread, 0, concat_in_thread, 0)) error("Thread creation failed concat");
			}
			//concat_in_thread(0);
			if (pthread_create(&detect_thread, 0, detect_in_thread, 0)) error("Thread creation failed");

			//if (nms) do_nms_obj(local_dets, local_nboxes, l.classes, nms);    // bad results
			if (nms) {
				if (l.nms_kind == DEFAULT_NMS) do_nms_sort(local_dets, local_nboxes, l.classes, nms);
				else diounms_sort(local_dets, local_nboxes, l.classes, nms, l.nms_kind, l.beta_nms);
			}

			//printf("\033[2J");
			//printf("\033[1;1H");
			//printf("\nFPS:%.1f\n", fps);
			//printf("Objects:\n\n");

			++frame_id;
			if (demo_json_port > 0) {
				int timeout = 400000;
				send_json(local_dets, local_nboxes, l.classes, demo_names, frame_id, demo_json_port, timeout);
			}

			//char *http_post_server = "webhook.site/898bbd9b-0ddd-49cf-b81d-1f56be98d870";
			if (http_post_host && !send_http_post_once) {
				int timeout = 3;            // 3 seconds
				int http_post_port = 80;    // 443 https, 80 http
				if (send_http_post_request(http_post_host, http_post_port, filename,
					local_dets, nboxes, classes, names, frame_id, ext_output, timeout))
				{
					if (time_limit_sec > 0) send_http_post_once = 1;
				}
			}
			if (rtp_socket) {
				memset(finalRectInfo, 0, sizeof(finalRectInfo));
			}
			if (tcp_socket) {
				memset(finalRectInfo, 0, sizeof(finalRectInfo));
			}
			draw_detections_cv_v3(show_img, local_dets, local_nboxes, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, rectInfo, rtp_socket, tcp_socket);
			if (rtp_socket) {
				if (rectInfo[0] != 0) {
					//printf("%s\n",rectInfo);
					pthread_mutex_lock(&mutex_lock);
					strcpy(finalRectInfo, rectInfo);
					rect_flag = 1;
					pthread_mutex_unlock(&mutex_lock);
				}
				else {
					pthread_mutex_lock(&mutex_lock);
					finalRectInfo[0] = 0x10;
					rect_flag = 0;
					pthread_mutex_unlock(&mutex_lock);
				}
			}
			if (tcp_socket) {
				if (rectInfo[0] != 0) {
					//printf("%s\n",rectInfo);
					pthread_mutex_lock(&mutex_lock);
					strcpy(finalRectInfo, rectInfo);
					rect_flag = 1;
					pthread_mutex_unlock(&mutex_lock);
				}
				else {
					pthread_mutex_lock(&mutex_lock);
					finalRectInfo[0] = 0x10;
					rect_flag = 0;
					pthread_mutex_unlock(&mutex_lock);
				}
			}
			free_detections(local_dets, local_nboxes);

			//printf("\nFPS:%.1f\n", fps);

			if (!prefix) {
				if (!dont_show) {
					show_image_mat(show_img, "Demo");
					int c = wait_key_cv(1);
					if (c == 10) {
						if (frame_skip == 0) frame_skip = 60;
						else if (frame_skip == 4) frame_skip = 0;
						else if (frame_skip == 60) frame_skip = 4;
						else frame_skip = 0;
					}
					else if (c == 27 || c == 1048603) // ESC - exit (OpenCV 2.x / 3.x)
					{
						flag_exit = 1;
					}
				}
			}
			else {
				char buff[256];
				sprintf(buff, "%s_%08d.jpg", prefix, count);
				if (show_img) save_cv_jpg(show_img, buff);
			}

			// if you run it with param -mjpeg_port 8090  then open URL in your web-browser: http://localhost:8090
			if (mjpeg_port > 0 && show_img) {
				int port = mjpeg_port;
				int timeout = 400000;
				int jpeg_quality = 40;    // 1 - 100
				send_mjpeg(show_img, port, timeout, jpeg_quality);
			}

			// save video file
			if (output_video_writer && show_img) {
				write_frame_cv(output_video_writer, show_img);
				printf("\n cvWriteFrame \n");
			}

			if (demo_multi_channel_flag == MULTI_TEST || demo_multi_channel_flag == 0) {
				pthread_join(fetch_thread[0], 0);
			}
			else if (demo_multi_channel_flag == MULTI_VIDEO) {
				printf("\nThread joining..1\n");
				pthread_join(fetch_thread[0], 0);
				/*if (demo_multi_channel > 1) {
					printf("\nThread joining..2\n");
					pthread_join(fetch_thread[1], 0);
				}
				if (demo_multi_channel > 2) {
					printf("\nThread joining..3\n");
					pthread_join(fetch_thread[2], 0);
				}
				if (demo_multi_channel > 3) {
					printf("\nThread joining..4\n");
					pthread_join(fetch_thread[3], 0);
				}*/
				//pthread_join(concat_thread, 0);
			}
			pthread_join(detect_thread, 0);

			if (time_limit_sec > 0 && (get_time_point() - start_time_lim) / 1000000 > time_limit_sec) {
				printf(" start_time_lim = %f, get_time_point() = %f, time spent = %f \n", start_time_lim, get_time_point(), get_time_point() - start_time_lim);
				break;
			}

			if (flag_exit == 1) break;

			if (delay == 0) {
				release_mat(&show_img);
				show_img = det_img;
			}
			det_img = in_img;
			det_s = in_s;
		}
		--delay;
		if (delay < 0) {
			delay = frame_skip;

			//double after = get_wall_time();
			//float curr = 1./(after - before);
			double after = get_time_point();    // more accurate time measurements
			float curr = 1000000. / (after - before);
			fps = curr;
			before = after;
		}
	}

	pthread_join(socket_thread, 0);
	printf("input video stream closed. \n");
	if (output_video_writer) {
		release_video_writer(&output_video_writer);
		printf("output_video_writer closed. \n");
	}

	// free memory
	free_image(in_s);
	free_detections(dets, nboxes);

	free(avg);
	for (j = 0; j < NFRAMES; ++j) free(predictions[j]);
	demo_index = (NFRAMES + demo_index - 1) % NFRAMES;
	for (j = 0; j < NFRAMES; ++j) {
		release_mat(&cv_images[j]);
	}

	free_ptrs((void **)names, net.layers[net.n - 1].classes);

	int i;
	const int nsize = 8;
	for (j = 0; j < nsize; ++j) {
		for (i = 32; i < 127; ++i) {
			free_image(alphabet[j][i]);
		}
		free(alphabet[j]);
	}
	free(alphabet);
	free_network(net);
	//cudaProfilerStop();
	}
#else
void demo(char *cfgfile, char *weightfile, float thresh, float hier_thresh, int cam_index, const char *filename, char **names, int classes,
	int frame_skip, char *prefix, char *out_filename, int mjpeg_port, int json_port, int dont_show, int ext_output, int letter_box_in, int time_limit_sec, char *http_post_host, int multi_channel_flag, int multi_channel, int rtp_socket, int tcp_socket)
{
	fprintf(stderr, "Demo needs OpenCV for webcam images.\n");
}
#endif
