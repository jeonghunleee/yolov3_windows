// C
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <direct.h>
// C++
#include <iostream>
#include <string>

#define COMMAND_PORT 6000
#define TCP_COMMON_PORT(x) (6001+x)
#define RTP_COMMON_PORT(x) (5000+x)

#define CHECK_CONNECTION_REQUEST 0X10
#define CHECK_CONNECTION_RESPONSE 0X11
#define MAIN_CONNECTION_REQUEST 0X20
#define MAIN_CONNECTION_RESPONSE 0X21
#define INSTANCE_CONNECTION_REQUEST 0X30
#define INSTANCE_CONNECTION_RESPONSE 0X31
#define EVENT_REPORT 0x07
#define EVENT_REPORT_START 0x40
#define EVENT_REPORT_STOP 0x41
#define STX 0x02
#define ETX 0x03

#define MAX_CHANNEL 10
//VAR
bool programDone = false;

//THREAD
pthread_mutex_t mutex_lock;
pthread_t instance_thread[10];
//////////////////////////////

//SOCKET
#pragma comment(lib,"ws2_32.lib")
#define BUF_LEN 1024

char buffer[BUF_LEN];
char msg[BUF_LEN - 3];
int msgSize = 0;
WSADATA wsaData;
struct sockaddr_in server_addr, client_addr;
int server_fd, client_fd;
int len, msg_size;
char temp[20];
DWORD t_out;
bool reuseflag = true;
void initializeSocket();
int parseMsg(char* buffer, int msg_size, char *message);
void sendChar(int client_fd, char msg);
void sendMsg(int client_fd, char* msg, int type);
int sock_process(char* msg);
////////////////////////////////
int usingFlag = 0;
//instance
int portList[MAX_CHANNEL] = { 0, };
int isWorking = 0;
int m_channel;
char rtsp_addr[MAX_CHANNEL][50] = { 0, };

void* instance_in_thread(void *arg) {
	//rtsp://admin:admin@192.168.0.5:554/1/stream2
	int channel = *(int *)arg;
	char port[6] = { 0, };
	sprintf(port, "%d", TCP_COMMON_PORT(channel * 10));
	printf("thread function entry, addr : %s\n", rtsp_addr[channel]);
	std::string myString = "darknet.exe detector demo data/obj_person.data data/yolov3_person.cfg yolov3_person_good.weights -i 0 -thresh 0.5 -dont_show ";
	myString.append(rtsp_addr[channel]);
	//myString.append("2");
	myString.append(" -tcp_port ");
	myString.append(port);
	printf("function \'system\' called with addr %s", rtsp_addr[channel]);
	//chdir("C:\\Users\\USER\\Desktop\\yolov3_windows\\build\\darknet\\x64");
	chdir("C:\\yolov3_windows\\build\\darknet\\x64");
	system(myString.c_str());

	printf("system done! \n");
	pthread_mutex_lock(&mutex_lock);
	portList[channel] = 0;
	pthread_mutex_unlock(&mutex_lock);
	for (int a = 0; a < MAX_CHANNEL; a++) {
		if (portList[a]) {
			printf("MAIN CHANNEL %d WORKING\n", a);
		}
		else {
			printf("MAIN CHANNEL %d NOT WORKING\n", a);
		}
	}
	return 0;
}



//system("darknet.exe detector demo cfg/coco.data yolov3.cfg yolov3.weights -i 0 -thresh 0.5 rtp://127.0.0.1:5004");
int main(int argc, void **argv) {
	pthread_mutex_init(&mutex_lock, NULL);
	initializeSocket();
	while (!programDone) {

		isWorking = 0;

		memset(buffer, 0x00, sizeof(buffer));
		memset(msg, 0x00, sizeof(msg));
		msgSize = recv(client_fd, buffer, BUF_LEN, 0);

		if (msgSize < 0) {
			//printf("Got nothing \n");
			int err = WSAGetLastError();
			if (err == WSAETIMEDOUT) continue;
			else if (err == WSAECONNRESET) {
				printf("Main Server : Connection abnormally closed! retry to connect!\n");
				client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
				inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));
				printf("Main Server : %s client connected.\n", temp);
				setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&t_out, sizeof(t_out));
				setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&reuseflag, sizeof(reuseflag));
			}
		}
		else if (msgSize == 0) {
			printf("Main Server :Connection closed. retrying to connect\n");
			client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
			inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));
			printf("Main Server : %s client connected.\n", temp);
			setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)& t_out, sizeof(t_out));
			setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&reuseflag, sizeof(reuseflag));
		}

		if (-1 == parseMsg(buffer, msg_size, msg)) {
			printf("Parsing Error, check the protocol. \n");
		}
		else {
			/*printf("buffer : %s", buffer);
			printf("msg : %s", msg);*/
			if (!sock_process(msg)) {
				printf("Full channels working \n");
			}
		}
	}

	for (int i = 0; i < MAX_CHANNEL; i++) {
		if (portList[i]) pthread_join(instance_thread[i], 0);
	}
}


int sock_process(char* message) {
	if (message[0] == CHECK_CONNECTION_REQUEST) {
		sendChar(client_fd, CHECK_CONNECTION_RESPONSE);
	}
	else if (message[0] == MAIN_CONNECTION_REQUEST) {

		for (m_channel = 0; m_channel < MAX_CHANNEL; m_channel++) {
			if (portList[m_channel] == 0) break;
		}

		printf("main : channel %d empty\n", m_channel);
		//Create Instance
		if (m_channel == 11) {
			//sendChar(client_fd, NACK);
			printf("main : full of channel \n");
			return 0;
		}

		for (int i = 0; message[i]; i++) {
			rtsp_addr[m_channel][i] = message[i + 1];
		}

		printf("main : addr %s received\n", rtsp_addr[m_channel]);
		char temp[6];
		itoa(TCP_COMMON_PORT(m_channel * 10), temp, 10);
		printf("main : msg sending.. port:%s\n", temp);
		printf("Trying to Create YOLO Instance.. \n");
		usingFlag = 0;
		if (pthread_create(&instance_thread[m_channel], 0, instance_in_thread, &m_channel)) printf("Thread creation failed\n");
		sendMsg(client_fd, temp, MAIN_CONNECTION_RESPONSE);
		pthread_mutex_lock(&mutex_lock);
		portList[m_channel] = TCP_COMMON_PORT(m_channel * 10);
		pthread_mutex_unlock(&mutex_lock);

	}
	else if (message[0] == 99) {
		char temp[6];
		itoa(TCP_COMMON_PORT(5 * 10), temp, 10);
		printf("main : msg sending.. port:%s\n", temp);
		//sendMsg(client_fd, temp, OPEN);
	}
	return 1;
}

void initializeSocket() {

	WSAStartup(MAKEWORD(2, 2), &wsaData);
	t_out = 3000;

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{// 소켓 생성
		printf("Server : Can't open stream socket\n");
		exit(0);
	}
	memset(&server_addr, 0x00, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(COMMAND_PORT);
	//server_addr 셋팅

	if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{//bind() 호출
		printf("Server : Can't bind local address.\n");
		exit(0);
	}

	if (listen(server_fd, 5) < 0)
	{//소켓을 수동 대기모드로 설정
		printf("Server : Can't listening connect.\n");
		exit(0);
	}

	printf("Server : wating connection request.\n");
	len = sizeof(client_addr);
	client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
	setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&t_out, sizeof(t_out));
	setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&reuseflag, sizeof(reuseflag));
	if (client_fd < 0)
	{
		printf("Server: accept failed.\n");
		exit(0);
	}
	inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, temp, sizeof(temp));
	printf("Server : %s client connected.\n", temp);
	return;
}

int parseMsg(char* buffer, int msg_size, char *message) {
	char i = 0;
	if (buffer[0] == STX) {
		for (; i < buffer[1]; i++) {
			message[i] = buffer[2 + i];
		}
		if (buffer[2 + i] == ETX) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 0;
	}
	return 1;
}

void sendChar(int client_fd, char message) {
	char temp[BUF_LEN];
	memset(temp, 0x00, sizeof(temp));
	temp[0] = STX;
	temp[1] = 0x01;
	temp[2] = message;
	temp[strlen(temp)] = ETX;
	send(client_fd, temp, strlen(temp) + 1, 0);
}
void sendMsg(int client_fd, char* message, int type) {
	char temp[BUF_LEN];
	memset(temp, 0x00, sizeof(temp));
	temp[0] = STX;

	if (type == MAIN_CONNECTION_RESPONSE) {
		temp[1] = strlen(message) + 1;
		temp[2] = MAIN_CONNECTION_RESPONSE;
		strcat(temp, message);
		temp[strlen(temp)] = ETX;
	}
	else {
		temp[1] = 1;
		temp[2] = ERROR;
		temp[3] = ETX;
	}

	printf(" LENGTH : %d, TOTAL : %d message : %s\n", temp[1], strlen(temp), temp);
	//write(client_fd, temp, sizeof(temp));
	send(client_fd, temp, strlen(temp) + 1, 0);
}